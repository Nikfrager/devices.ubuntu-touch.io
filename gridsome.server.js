/**
 * Ubuntu Touch devices website
 * Copyright (C) 2021 UBports Foundation <info@ubports.com>
 * Copyright (C) 2021 Jan Sprinz <neo@neothethird.de>
 * Copyright (C) 2021 Riccardo Riccio <rickyriccio@zoho.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const requireMaybe = require("require-maybe");

const portStatus = require("./data/portStatus.json");
const deviceInfo = require("./data/deviceInfo.json");
const progressStages = require("./data/progressStages.json");
const installerData = requireMaybe("./data/installerData.json") || [];

if (!installerData.length) {
  console.log(
    "\x1b[33m%s\x1b[0m",
    "Installer data unavailable at build time.\n",
    "You should consider running one of the following commands instead:\n",
    "npm run develop : Fetch data and run in development mode\n",
    "npm run build : Fetch data and build site for production mode\n",
    "npm run get-data : Only fetch data"
  );
}

module.exports = function(api) {
  api.onCreateNode(options => {
    if (options.internal.typeName === "Device") {
      // Set codename from file name
      options.codename = options.fileInfo.name;

      // Calculate porting progress from feature matrix ( use maturity field as fallback )
      if (options.portStatus) {
        let featureScale = ["x", "-", "?", "+-", "+"],
          notFound = "",
          totalWeight = 0,
          currentWeight = 0,
          currentStageIndex = progressStages.length - 1; // Daily-driver ready

        for (let portCategory in portStatus) {
          let categoryFeatures = options.portStatus.find(
            el => el.categoryName == portCategory
          ).features;

          for (let feature of portStatus[portCategory]) {
            let graphQlFeature = categoryFeatures.find(
              el => el.id == feature.id
            );

            if (graphQlFeature) {
              graphQlFeature.name = feature.name;
              delete graphQlFeature.id;
            } else {
              notFound += feature.name + "\n";

              categoryFeatures.push({
                name: feature.name,
                value: feature.default ? feature.default : "?"
              });

              graphQlFeature = categoryFeatures.find(
                el => el.name == feature.name
              );
            }

            if (
              feature.global &&
              featureScale.indexOf(feature.global) <=
                featureScale.indexOf(graphQlFeature.value)
            ) {
              graphQlFeature.value = feature.global;
              graphQlFeature.bugTracker = feature.bugTracker
                ? feature.bugTracker
                : "";
            }

            // Calculate progress (from weights)
            totalWeight += graphQlFeature.value != "x" ? feature.weight : 0;
            currentWeight += graphQlFeature.value == "+" ? feature.weight : 0;

            // Calculate progress stage
            if (graphQlFeature.value == "-" || graphQlFeature.value == "+-") {
              let featureStageIndex = progressStages.indexOf(feature.stage);
              if (
                currentStageIndex >= featureStageIndex &&
                featureStageIndex > 0
              ) {
                currentStageIndex = featureStageIndex - 1;
              }
            }
          }
        }

        if (notFound.length > 0) {
          console.log(
            "\x1b[33m%s\x1b[0m",
            options.name + " has missing features in feature matrix.",
            "\nThe following features are missing:\n" + notFound
          );
        }

        options.progress =
          Math.round((1000 * currentWeight) / totalWeight) / 10;
        options.progressStage = progressStages[currentStageIndex];

        // Fallback from maturity
      } else {
        options.progress = options.maturity * 100;
        options.progressStage = progressStages[0];
        console.log(
          "\x1b[33m%s\x1b[0m",
          "Using maturity as fallback for: " + options.name
        );
      }

      // Elaborate device info from IDs
      if (options.deviceInfo) {
        options.deviceInfo.forEach(el => {
          el.name = deviceInfo.find(info => info.id == el.id).name;
          delete el.id;
        });
      }

      // Import installer compatibility data
      let deviceCodenameAlias = options.installerAlias
        ? options.installerAlias
        : options.codename;
      let deviceInstaller = installerData.some(
        el =>
          el.codename == deviceCodenameAlias &&
          el.operating_systems.includes("Ubuntu Touch")
      );
      options.noInstall = !deviceInstaller;
    }
  });
};
