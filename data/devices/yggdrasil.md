---
name: "Volla Phone"
deviceType: "phone"
buyLink: "https://volla.online/de/shop/"
tag: "promoted"
portStatus:
  - categoryName: "Actors"
    features:
      - id: "manualBrightness"
        value: "+"
      - id: "notificationLed"
        value: "x"
      - id: "torchlight"
        value: "+"
      - id: "vibration"
        value: "+"
        bugTracker: "https://github.com/HelloVolla/ubuntu-touch-beta-tests/issues/77" # keyboard haptics too subtle
  - categoryName: "Camera"
    features:
      - id: "flashlight"
        value: "+"
      - id: "photo"
        value: "+"
      - id: "video"
        value: "+"
      - id: "switchCamera"
        value: "+"
  - categoryName: "Cellular"
    features:
      - id: "carrierInfo"
        value: "+"
      - id: "dataConnection"
        value: "+-"
        bugTracker: "https://github.com/ubports/ubuntu-touch/issues/1657" # not working on some providers ootb
      - id: "dualSim"
        value: "+-"
        bugTracker: "https://github.com/HelloVolla/ubuntu-touch-beta-tests/issues/93" # cellular data doesn't work on SIM 2
      - id: "calls"
        value: "+"
      - id: "mms"
        value: "+"
      - id: "pinUnlock"
        value: "+"
      - id: "sms"
        value: "+"
      - id: "audioRoutings"
        value: "+"
      - id: "voiceCall"
        value: "+"
  - categoryName: "Endurance"
    features:
      - id: "batteryLifetimeTest"
        value: "+"
      - id: "noRebootTest"
        value: "-"
        bugTracker: "https://github.com/ubports/ubuntu-touch/issues/1629" # carrier loss after ~24 hours
  - categoryName: "GPU"
    features:
      - id: "uiBoot"
        value: "+"
      - id: "videoAcceleration"
        value: "+"
  - categoryName: "Misc"
    features:
      - id: "anboxPatches"
        value: "+"
      - id: "apparmorPatches"
        value: "+"
      - id: "batteryPercentage"
        value: "+"
      - id: "offlineCharging"
        value: "-"
        bugTracker: "https://github.com/Halium/initramfs-tools-halium/pull/22" # broken for Android 8+ devices, linked temporary workaround
      - id: "onlineCharging"
        value: "+"
      - id: "recoveryImage"
        value: "+"
      - id: "factoryReset"
        value: "+"
      - id: "rtcTime"
        value: "+"
      - id: "sdCard"
        value: "+"
      - id: "shutdown"
        value: "+"
      - id: "wirelessCharging"
        value: "+"
      - id: "wirelessExternalMonitor"
        value: "-"
        bugTracker: "https://github.com/HelloVolla/ubuntu-touch-beta-tests/issues/92" # aethercast not working
  - categoryName: "Network"
    features:
      - id: "bluetooth"
        value: "+"
        bugTracker: "https://github.com/HelloVolla/ubuntu-touch-beta-tests/issues/69" # doesn't work with newer (BT 5) devices
      - id: "flightMode"
        value: "+"
      - id: "hotspot"
        value: "+"
      - id: "nfc"
        value: "-"
      - id: "wifi"
        value: "+"
  - categoryName: "Sensors"
    features:
      - id: "autoBrightness"
        value: "+-"
        bugTracker: "https://github.com/ubports/repowerd/pull/14" # ALS sensor reports data, just not used by MW
      - id: "fingerprint"
        value: "+"
      - id: "gps"
        value: "+"
      - id: "proximity"
        value: "+"
      - id: "rotation"
        value: "+"
      - id: "touchscreen"
        value: "+"
  - categoryName: "Sound"
    features:
      - id: "earphones"
        value: "+"
      - id: "loudspeaker"
        value: "+"
      - id: "microphone"
        value: "+"
      - id: "volumeControl"
        value: "+"
  - categoryName: "USB"
    features:
      - id: "mtp"
        value: "+"
      - id: "adb"
        value: "+"
      - id: "wiredExternalMonitor"
        value: "x"
deviceInfo:
  - id: "cpu"
    value: "Octa-core ARM Cortex-A53 (4x 2.0 GHz + 4x 1.5 GHz cores)"
  - id: "chipset"
    value: "MediaTek Helio P23, MT6763V"
  - id: "gpu"
    value: "ARM Mali-G71 MP2 @ 770 MHz, 2 cores"
  - id: "rom"
    value: "64 GB, eMMC"
  - id: "ram"
    value: "4 GB, DDR3"
  - id: "android"
    value: "9.0 (Pie)"
  - id: "battery"
    value: "4700 mAh, 18.1 Wh, Li-Polymer"
  - id: "display"
    value: '6.3" IPS, 1080 x 2340 (409 PPI), V-notch, Rounded corners'
  - id: "rearCamera"
    value: "16MP (f/2.0, 1080p30 video) + 2MP (for bokeh/depth), PDAF, LED flash"
  - id: "frontCamera"
    value: "16MP"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "157 mm x 75.1 mm x 9.65 mm"
  - id: "weight"
    value: "185 g"
contributors:
  - name: TheKit
    forum: https://forums.ubports.com/user/thekit
    photo: ""
  - name: Deathmist
    forum: https://forums.ubports.com/user/deathmist
    photo: https://forums.ubports.com/assets/uploads/profile/3171-profileavatar-1613145487767.png
externalLinks:
  - name: "Telegram"
    link: "https://t.me/utonvolla"
    icon: "telegram"
  - name: "Device Subforum"
    link: "https://forums.ubports.com/category/90/vollaphone"
    icon: "yumi"
  - name: "Report a bug"
    link: "https://github.com/HelloVolla/ubuntu-touch-beta-tests/issues"
    icon: "github"
  - name: "Device source"
    link: "https://github.com/HelloVolla/android_device_volla_yggdrasil/tree/halium-9.0"
    icon: "github"
  - name: "Kernel source"
    link: "https://github.com/HelloVolla/android_kernel_volla_mt6763/tree/halium-9.0"
    icon: "github"
  - name: "Halium build manifest"
    link: "https://github.com/Halium/halium-devices/blob/halium-9.0/manifests/volla_yggdrasil.xml"
    icon: "github"
---
