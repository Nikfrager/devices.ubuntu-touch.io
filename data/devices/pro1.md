---
name: "F(x)tec Pro1"
deviceType: "phone"
image: "https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1603110770/ysxni2y7ulluzwlnw3fl.jpg"
maturity: .9
buyLink: 'https://store.fxtec.com/product/fxtec-pro1/'
---

The [F(x)tec Pro1](https://www.fxtec.com/pro1) is the first Ubuntu Touch phone with a physical Keyboard!
