---
name: "Google Pixel 2 and Pixel 2 XL"
deviceType: "phone"
portType: 'Halium 9.0'
image: "https://wiki.lineageos.org/images/devices/walleye.png"
maturity: .5
externalLinks:
  - name: 'Forum Post'
    link: 'https://forums.ubports.com/category/67/google-pixel-2-2-xl'
    icon: 'yumi'
---

## Known issues
- No sound
- Fingerprint sensor operations crashing
- Instead of shutdown device reoboots often
