---
name: "Xiaomi Mi A2"
comment: "community device"
deviceType: "phone"
maturity: .95
installerAlias: "jasmine_sprout"
---

The Xiaomi Mi A2 is a mid-range smartphone co-developed by Xiaomi and Google as part of Android One program.
It ended up support with android 10, but now you can install the newest ubuntu touch on it.

### Device specifications

|               Component | Details                                                         |
| ----------------------: | --------------------------------------------------------------- |
|                 Chipset | Qualcomm SDM665 Snapdragon 660                                  |
|                     CPU | Octa-core (4x2.2 GHz Kryo 260 Gold & 4x1.8 GHz Kryo 260 Silver) |
|            Architecture | arm64                                                           |
|                     GPU | Adreno 512                                                      |
|                 Display | 1080x2160                                                       |
|                 Storage | 32GB / 64 GB / 128 GB                                           |
| Shipped Android Version | 8.1 (Oreo)                                                      |
|                  Memory | 4 GB / 6 GB                                                     |
|                 Cameras | 20 MP + 12 MP, LED flash<br>20 MP, LED flash                    |
|                 Battery | 3000mAh                                                         |
|              Dimensions | 158.7 x 75.4 x 7.3 mm                                           |
|                  Weight | 166 g                                                           |
|            Release Date | July 2018                                                       |

### Maintainer(s)

shoukolate

### Contributor(s)

nebrassy, robante15, just-carlod, NotKit, Flohack, erfanoabdi

### Source repos

https://github.com/ubports-xiaomi-sdm660

[Telegram chat](https://t.me/shoukolab)
